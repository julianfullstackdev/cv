# Julián Álvarez CV

Este es un proyecto simple de mi CV.

## ¿Qué usa?

Este proyecto usa principalmente NextJs y Styled-components, ambas implementan como lenguaje JavaScript y ReactJs como librería, esta hosteado en Now, de Zeit.

El CSS utiliza Grid y no usé frameworks.

### Correr localmente `usando npm`

Instala con [`npm`](https://github.com/segmentio/create-next-app) los paquetes para correr el proyecto, luego corre con npm y ve a localhost:3000 :)

```bash
npm install
# Luego
npm run dev
```

### Descargar proyecto

Para eso puedes usar git:

```bash
git clone https://gitlab.com/julianfullstackdev/cv
```

### Fun facts

Este proyecto no usa media queries, pero es responsive 😲 \
Es muy rápida --> [`Google Insights`](https://developers.google.com/speed/pagespeed/insights/?hl=es&url=cv.julianux.com) \
Descubré Now, el [`deployment`](https://cv.julianux.com/_src) es público también 😊

MIT License
